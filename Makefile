

serve:
	mdbook serve

build:
	rm -r book
	mdbook build

pages: build
	cd book && wrangler pages publish . --project-name wiki
